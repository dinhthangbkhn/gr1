# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlerFilmItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class Film(scrapy.Item):
    imdb_id = scrapy.Field()
    name = scrapy.Field()
    casts = scrapy.Field()
    directors = scrapy.Field()
    release_date = scrapy.Field()
    length = scrapy.Field()
    rating = scrapy.Field()
    rating_numbers = scrapy.Field()
    genres = scrapy.Field()
    critics = scrapy.Field()
    storyline = scrapy.Field()
    budget = scrapy.Field()
    user_review = scrapy.Field()
    typeItem = scrapy.Field()


class Person(scrapy.Item):
    imdb_id = scrapy.Field()
    name = scrapy.Field()
    filmography = scrapy.Field()
    starmeter = scrapy.Field()
    typeItem = scrapy.Field()

class Userreview(scrapy.Item):
    film_imdb_id = scrapy.Field()
    content = scrapy.Field()
    typeItem = scrapy.Field()