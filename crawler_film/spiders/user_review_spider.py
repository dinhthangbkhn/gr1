from ..items import Userreview

import scrapy

def createLink():
    listline = []
    listlink = []
    with open("links.csv","r") as file:
        listline = file.readlines()
    for line in listline[1:-1]:
        currentlist = line.split(",")
        listlink.append("http://www.imdb.com/title/tt"+currentlist[1]+"/reviews")
    return listlink

class UserreviewSpider(scrapy.Spider):
    name = "review"

    def start_requests(self):
        links = createLink()
        for link in links:
            yield scrapy.Request(link,self.parser_page)
    
    def parser_page(self, response):
        
        listReviews = response.xpath("//div[@id='tn15content']/p").extract()
        listReviews.pop() #remove "Add another review"

        for review in listReviews:
            item = Userreview()
            item["typeItem"] = "user_review"
            item["film_imdb_id"] = response.request.url
            item["content"] = review
            yield item
        
        # nextLink = response.xpath("//").extract_first()

