import scrapy
from ..items import Person
from ..items import Film

class FirstSpider(scrapy.Spider):
    name = "imdb"
    listPersonId = []
    currentFilmLink = ""
    currentPersonLink = ""

    def start_requests(self):
        links = createLink()
        for link in links:
            # FirstSpider.currentFilmLink = link
            yield scrapy.Request(link,self.parse_film)
    
    def parse_film(self, response):
        print("\n\n\nLIST:"+ str(len(FirstSpider.listPersonId))+"\n\n")
        item = Film()

        item["name"] = response.xpath("//div[@class='titleBar']/div[@class='title_wrapper']/h1/text()").extract_first()

        item["imdb_id"] = response.request.url

        item["directors"] = response.xpath("//div[@class='credit_summary_item'][1]/span/a/@href").extract()

        item["casts"] = response.xpath("//div[@id='titleCast']/table[@class='cast_list']/tr/td/a/@href").extract()
        item["casts"] = list(set(item["casts"]))

        item["storyline"] = response.xpath("///div[@id='titleStoryLine']/div[@class='inline canwrap']/p/text()").extract_first()
        
        item["genres"] = response.xpath("//div[@class='see-more inline canwrap'][2]/a/text()").extract()
        
        item["typeItem"] = "film"
        
        item["budget"] = response.xpath("//div[@id='titleDetails']/div[@class='txt-block'][7]/text()").extract()[1]
        
        item["release_date"] =response.xpath("//div[@class='title_wrapper']/div[@class='subtext']/a[4]/text()").extract_first()
        
        item["length"] =  response.xpath("//div[@class='title_wrapper']/div[@class='subtext']/time/text()").extract_first()
        
        item["rating"] = response.xpath("//div[@class='ratingValue']/strong/span/text()").extract_first()
        
        item["rating_numbers"] = response.xpath("//div[@class='imdbRating']/a/span[@class='small']/text()").extract_first()

        yield item

        for cast in item["casts"]:
            if cast in self.listPersonId:
                continue
            else:
                FirstSpider.listPersonId.append(cast)
                # FirstSpider.currentPersonLink = cast
                requestCast = scrapy.Request("http://imdb.com/name/"+cast,self.parse_person)
                yield requestCast
        
        for director in item["directors"]:
            if director in self.listPersonId:
                continue
            else:
                FirstSpider.listPersonId.append(director)
                # FirstSpider.currentPersonLink = director
                requestDirector = scrapy.Request("http://imdb.com/name/"+director,self.parse_person)
                yield requestDirector

    
    def parse_person(self, response):
        # print("\n\n\nLIST:"+ str(len(FirstSpider.listPersonId))+"\n\n")
        item = Person()
        item["typeItem"] = "person"
        item["imdb_id"] = response.request.url
        item["starmeter"] = response.xpath("//div[@id='meterHeaderBox']/a[@id='meterRank']/text()").extract_first()
        item["name"] = response.xpath("//tbody/tr[1]/td[@id='overview-top']/h1[@class='header']/span[@class='itemprop']/text()").extract_first()
        item["filmography"] = response.xpath("//div[@class='filmo-category-section']/div/b/a").extract()
        yield item 


def createLink():
    listline = []
    listlink = []
    with open("links.csv","r") as file:
        listline = file.readlines()
    for line in listline[1:-1]:
        currentlist = line.split(",")
        listlink.append("http://www.imdb.com/title/tt"+currentlist[1])
    return listlink
