# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo

class CrawlerFilmPipeline(object):
    collection_name = ['film','person']

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        
        if item["typeItem"] == "person":
            item["imdb_id"] = filtId(item["imdb_id"])
            self.db[self.collection_name[1]].insert(dict(item))
        elif item["typeItem"] == "user_review":
            item["film_imdb_id"] = item["film_imdb_id"].split("/")[4]
            self.db["user_reviews"].insert(dict(item))
        else:
            item["imdb_id"] = filtId(item["imdb_id"])
            listCast = []
            for cast in item["casts"]:
                cast = filtPerson(cast)
                listCast.append(cast)
            item["casts"] = listCast

            listDirector = []
            for director in item["directors"]:
                director = filtPerson(director)
                listDirector.append(director)
            item["directors"] = listDirector
            
            self.db[self.collection_name[0]].insert(dict(item))
        return item

def filtId(link):
    return link.split("/")[4]

def filtPerson(link):
    return link.split("?")[0].split("/")[2]